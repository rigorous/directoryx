package org.directoryx.sync;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.directoryx.domain.Person;
import org.directoryx.domain.PersonMessage;
import org.directoryx.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.template.Neo4jOperations;
import org.springframework.data.neo4j.template.Neo4jTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Component
public class Receiver implements MessageListener {

    static Logger log = LoggerFactory.getLogger(Receiver.class);

    // Fails to be injected...
    // Injection of autowired dependencies failed; nested exception is
    // org.springframework.beans.factory.BeanCreationException: Could not autowire field:
    @Autowired
    private PersonService personService;

    public void onMessage(Message message) {
        List<PersonMessage> peopleToUpdate = new ArrayList<PersonMessage>();

        log.info("Retrieving updates...");
        Gson gson  = new GsonBuilder().create();
        String msg = new String(message.getBody());
        log.info("msg = " + msg);

        // Magic thing to help gson deserialize a *List* of Person type objects.
        // We don't actually receive a Person entity, just a Data Transfer Person Thingy
        // TODO: Create a PersonDTO object

        Type listType = new TypeToken<ArrayList<PersonMessage>>() {}.getType();
        peopleToUpdate = gson.fromJson(msg, listType);
        log.info("Successfully retrieved people!");


        for (PersonMessage pMessage: peopleToUpdate) {
            //find person's manager
            Person manager = personService.findByEmplId(pMessage.getMgrID());

            //associate person with manager (make relationship)
            Person person = personService.findByUserId(pMessage.getUserId());
            if (person == null) {
                person = new Person();
                person.setUserId(pMessage.getUserId());
            }

            //updates person's fields
            person.setManager(manager);
            person.setFirstName(pMessage.getFirstName());
            person.setLastName(pMessage.getLastName());
            person.setTitle(pMessage.getJobTitle());

            //save person
            personService.savePerson(person);
        }


    }

}
