package org.directoryx.domain;

import org.neo4j.ogm.annotation.Transient;

import java.util.ArrayList;

public class Project extends Entity {

    // @TODO either replace team and projectManager with @RelatedTo object or remove them

    private String name;
    private String description;
    private String startDate;
    private String endDate;

    public Project() { }

    public String getName() {
        return name;
    }

    public void setName(String projectName) {
        this.name = projectName;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
