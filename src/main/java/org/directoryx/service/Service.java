package org.directoryx.service;

public interface Service<T> {

    Iterable<T> findAll();

    T find(Long id);

    void delete(Long id);

    T createOrUpdate(T object);

    //Iterable<T> search(String searchTerm);
}
